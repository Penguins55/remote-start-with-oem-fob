// Settings defined here:

// Change pin numbers if needed:
#define       TrunkButtonPin    2 // IN - needs to be an intterupt pin
#define       BrakeInPin        10 // IN - no intterupt
#define       VoltMtrPin        A0 // ANALOG IN
#define       RunPower          4 // OUT - to relay
#define       Acc1Power         5 // OUT - to relay
#define       Acc2Power         6 // OUT - to relay
#define       StarterPower      7 // OUT - to relay
#define       TrunkLatchPin     8 // Trunk latch output pin
#define       RearDefrostPin    9 // OUT - to relay

#define       OUTPUT_ON         HIGH
#define       OUTPUT_OFF        LOW
#define       R1                100000.00 // Value in ohms
#define       R2                10000.00 // Value in ohms
#define       PreStartTime      4000  // in millisecs - since the chevy leaks fuel pressure when off, let it build up before starting
#define       MaxStarterTime    8000 // in millisecs
#define       RunTimeInMins     25 // in minutes
#define       ButtonPressWait   60 // in millisecs DIVIDED BY 100 (e.g. 10 = 1 second)
#define       DefrostDelay      2000 // in millisecs, can't be greater than 1 minute (60000 millisecs)
// End of settings.


bool BrakeCancelsStart = true;
int TrunkPressCount = 0;
int ButtonPressLoopCount = 0; // resets on every button press

void setup() {
  // put your setup code here, to run once:
  pinMode(RunPower,OUTPUT);
  pinMode(Acc1Power,OUTPUT);
  pinMode(Acc2Power,OUTPUT);
  pinMode(StarterPower,OUTPUT);
  pinMode(TrunkButtonPin,INPUT);
  pinMode(BrakeInPin,INPUT);
  pinMode(TrunkLatchPin,OUTPUT);
  pinMode(RearDefrostPin,OUTPUT);

  digitalWrite(RunPower,OUTPUT_OFF);
  digitalWrite(Acc1Power,OUTPUT_OFF);
  digitalWrite(Acc2Power,OUTPUT_OFF);
  digitalWrite(StarterPower,OUTPUT_OFF);
  digitalWrite(TrunkLatchPin,OUTPUT_OFF);
  digitalWrite(RearDefrostPin,OUTPUT_OFF);

  Serial.begin(9600);
  Serial.println("Remote Starter v1");

  attachInterrupt(digitalPinToInterrupt(TrunkButtonPin), TrunkButtonPressed, RISING);
}

void TrunkButtonPressed() {
  if( TrunkPressCount > 0 && ButtonPressLoopCount < 5 )
    return; // too soon, must be bounce 
  TrunkPressCount = TrunkPressCount + 1;
  ButtonPressLoopCount = 0;
  Serial.println("Trunk Button Pressed.  ");
  Serial.println(VoltMeter());
}

float VoltMeter() {
  return ((analogRead(VoltMtrPin) * 5.00) / 1024.00) / (R2 / (R1 + R2));
}

bool BrakePressed() {
  if( digitalRead(BrakeInPin) == HIGH )
    return true;
  else
    return false;
}

bool IsRunning() {
  if( VoltMeter() > 13.00 )
    return true;
  else
    return false;
}

int StarterActiveDelay(int LengthOfTime) { // monitor for LengthOfTime and return early if need to abort 
  if( LengthOfTime == 0 )
    LengthOfTime = 1;
  while( LengthOfTime > 0 ) {
    if( IsRunning() )
      return 0; // STARTED!
    if( TrunkPressCount > 0 )
      return 2; // Cancelled
    if( BrakeCancelsStart == true && BrakePressed() == true )
      return 2; // Cancelled
    if( LengthOfTime >= 100 ) {
      LengthOfTime = LengthOfTime - 100;
      delay(100);
    } else
      LengthOfTime = 0;
  }
  return 1; // timed out
}

void Ignition(int IgnitionSwitch, bool state) {
  if( state == true )
    digitalWrite(IgnitionSwitch,OUTPUT_ON);
  else
    digitalWrite(IgnitionSwitch,OUTPUT_OFF);
}

int Start() {
  int Tmp;

  Serial.println("Starting....  ");

  Tmp = StarterActiveDelay(0);

  if( Tmp == 0 )  { // Already running, skip starting and allow for key removel.
    Ignition(RunPower,1);
    Ignition(Acc1Power,1);
    Ignition(Acc2Power,1);
    Serial.println("Already running, allowing for key removal.  ");
    return 0;
  }

  if( Tmp != 1 ) {
    Serial.println("Start Cancelled.  ");
    return 2; // user cancelled
  }

  Ignition(RunPower,1);

  switch( StarterActiveDelay(PreStartTime) ) {
    case 0: // car started before we engaged the starter, abort.
      Ignition(RunPower,0);
      Serial.println("User started before we could, aborting....  ");
      return 3;
      break;
    case 2: // user cancelled
      Ignition(RunPower,0);
      Serial.println("User Cancelled.  ");
      return 2;
      break;
  }

  Ignition(StarterPower,1); // engage starter

  switch( StarterActiveDelay(MaxStarterTime) ) {
    case 0: // Started
      Ignition(StarterPower,0);
      Ignition(Acc1Power,1);
      Ignition(Acc2Power,1);
      Serial.println("Started.  ");
      return 0;
      break;
    case 1: // Timed out
      Ignition(StarterPower,0);
      Ignition(RunPower,0);
      Serial.println("Timed out.  ");
      return 1;
      break;
    case 2: // user cancelled
      Ignition(StarterPower,0);
      Ignition(RunPower,0);
      Serial.println("User Cancelled.  ");
      return 2;
      break;
  }
}

void RunningTimer(bool Defrost) {
  int Timer = 0;
  int TmpTimer = 0;

  Serial.print("Running for ");
  Serial.print(RunTimeInMins);
  Serial.println(" minutes....  ");
  
  while( Timer <= RunTimeInMins ) {
    if( Timer > 1 && IsRunning() == false )
      break; // engine died
    if( TrunkPressCount > 0 )
      break; // Cancelled
    if( BrakePressed() == true )
      break; // Cancelled
    if( Timer == 0 && Defrost == true && TmpTimer >= DefrostDelay ) {
      digitalWrite(RearDefrostPin,OUTPUT_ON);
      delay(500);
      digitalWrite(RearDefrostPin,OUTPUT_OFF);
      Defrost = false;
      Serial.println("Defroster turned on.  ");
    }
    if( TmpTimer <= 600 ) {
      TmpTimer = TmpTimer + 1;
      delay(100);
      //Serial.print(".");
    } else {
      TmpTimer = 0;
      Timer = Timer + 1;
      Serial.println(Timer);
    }
  }
  Serial.println("Remote start timed out or was cancelled.  ");
  Ignition(Acc1Power,0); // Done.  Car off.
  Ignition(Acc2Power,0);
  Ignition(RunPower,0);
}

void loop() {
  // put your main code here, to run repeatedly:

  if( ButtonPressLoopCount >= ButtonPressWait ) {

    if( TrunkPressCount == 1 ) {
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
      Serial.println("Trunk Open.  ");
      digitalWrite(TrunkLatchPin,OUTPUT_ON);
      delay(1000);
      digitalWrite(TrunkLatchPin,OUTPUT_OFF);
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
    }

    if( TrunkPressCount == 3 ) {
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
      Serial.println("Start with no defroster.  ");
      BrakeCancelsStart = true;
      if( Start() == 0 )
        RunningTimer(false);
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
    }

    if( TrunkPressCount == 4 ) {
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
      Serial.println("Start and turn on defroster.  ");
      BrakeCancelsStart = true;
      if( Start() == 0 )
        RunningTimer(true);
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
    }

    if( TrunkPressCount == 12 && BrakePressed() ) {
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
      Serial.println("Start and allow to be driven until button pressed again.  ");
      BrakeCancelsStart = false;
      if( Start() == 0 )
        while( TrunkPressCount == 0 )
          delay(100);

      Serial.println("Button pressed, turn car off.  ");
      Ignition(Acc1Power,0); // Button pressed.  Car off.
      Ignition(Acc2Power,0);
      Ignition(RunPower,0);

      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
    }
  }

  if( TrunkPressCount > 0 ) {
    ButtonPressLoopCount = ButtonPressLoopCount + 1;
    if( ButtonPressLoopCount > ButtonPressWait ) {
      ButtonPressLoopCount = 0;
      TrunkPressCount = 0;
      Serial.println("Trunk button timeout, selection invalid.  ");
    }
  }

  if( TrunkPressCount == 0 && ButtonPressLoopCount != 0 )
    ButtonPressLoopCount = 0;

  if( TrunkPressCount > 0 )
    ButtonPressLoopCount = ButtonPressLoopCount + 1;

//  Serial.println(VoltMeter());

  delay(100);
}
